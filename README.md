# Systém pro optimální sledování živých streamů z výběhu slonů

Tato příloha obsahuje 3 hlavní adresáře a text práce [thesis.pdf](thesis.pdf).

| Adresář   | popis        |
|--------|---------------|
| 1. [implementation](implementation/) | adresář se zdrojovými kódy|
| 2. [data](data) | adresář s částí datových souborů|
| 3. [latex](latex/) | zdrojové kódy práce ve formátu LaTeX|





