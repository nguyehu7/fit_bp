\chapter{Teoretické zázemí}
V praktické části práce se využíval YOLOv8 model pro detekci objektů. K pochopení praktické části je nutné představit architekturu, která za stojí za YOLO modely. Navíc jsou představeny metriky, které slouží k vyhodnocení modelů detekce objektů. Vytváření heatmap používá metody perspektivní transformace, které jsou též představeny.
\section{YOLO}
Rodina YOLO (You Only Look Once) modelů začala s verzí YOLOv1, která byla v roce 2015 představena v práci~\cite{YOLO} od Redmona a spol. Model byl považován za průlomový v oblasti detekce objektů v reálném čase. Nový přístup YOLOv1 výrazně zvýšil rychlost detekce tím, že využil jedinou konvoluční síť k detekci bounding boxů (ohraničující obdélník) a klasifikaci tříd. S postupem času přinesla každá další verze drobná vylepšení. V roce 2023 byl společností Ultralytics představen nejnovější model YOLOv8, který je považován za současný state-of-the-art model detekce objektů~\cite{Jocher_Ultralytics_YOLO_2023, roboflow2023yolov8}.

\subsection{YOLOv1}
Autoři YOLOv1 Redmon a spol. navrhli architekturu, která využívala pouze jednu neuronovou síť k detekci objektů. Model dokázal podchytit sémantiku celého obrázku a navrhovat všechny bounding boxy najednou. Diagram je zobrazen na obrázku~\ref{fig:yo}. Vstupní obrázek byl nejdříve rozdělen na $ S\times S $ čtvercových buněk. Pro každou buňku bylo predikováno $B$ bounding boxů a $P_c$ pravděpodobností náležitosti objektu k nějaké třídě. 
Každý bounding box se skládá z 5 hodnot $x, y, w, h$ a $C$, kde $x$ a $y$ jsou normalizované souřadnice středu boxu vůči šířce a výšce obrázku a $w$ a $h$ jsou normalizované šířka a výška boxu. $C$ je míra spolehlivosti (confidence score), které odráží pravděpodobnost, že bounding box obsahuje objekt s určitou přesností překryvu. Autoři ji formálně definovali vztahem
\begin{equation}
    \text{Confidence} = \text{Pr(Object)} \times \text{IoU}^{\text{truth}}_{\text{pred}},
\end{equation}
kde $\text{Pr(Object)}$ je pravděpodobnost, že bounding obsahuje objekt a $\text{IoU}^{\text{truth}}_{\text{pred}}$ je IoU (Intersection over Union) mezi predikovaným bounding boxem a libovolným ground-truth bounding boxu (skutečný bounding box z anotace). 

\begin{figure}[!h]
        \centering
    \includegraphics[width=0.7\textwidth]{images/therory/model.pdf}
        \captionsetup{justification=centering}
	\caption[Obrázkový diagram YOLOv1.]{Obrázkový diagram YOLOv1.~\cite{YOLO}}
	\label{fig:yo}
\end{figure}

Jelikož model predikuje vysoký počet bounding boxů, je na konci sítě aplikován NMS (Non-Max Suppresion), který sloučí překrývající se bounding boxy do jednoho.


Architektura neuronové sítě YOLOv1 se skládá z 24 konvolučních vrstev následovanou 2 plně propojenými vrstvami (fully connected layer). Prvních 20 konvolučních vrstev spolu s jednou pooling vrstvou a jednou plně propojenou sítí bylo předtrénováno na ImageNet datasetu s velikostí snímků  224 $\times$ 224 px. Zbytek sítě, tedy 4 konvoluční vrstvy a 2 plně propojené, byly inicializovány náhodnými vahami. Následně byl modél dotrénován (fine-tuned) na PASCAL VOC datasetu s velikostí snímků 448 $\times$ 448 px. Všechny vrstvy krom poslední využívají \textit{Leaky RELU (Rectified Linear Unit)} aktivační funkci definovou vztahem

\begin{equation}
\phi(x) \leftarrow 
\begin{cases} 
x & \text{pro } x > 0,  \\
0,1x & \text{jinak.}
\end{cases}
\end{equation}


% \begin{figure}[!h]
%         \centering
% 	\includegraphics[width=1.0\textwidth]{images/therory/net.pdf}
%         \captionsetup{justification=centering}
% 	\caption[YOLOv1 architektura.]{YOLOv1 architektura.~\cite{YOLO}}
% 	\label{fig:net}
% \end{figure}

Model YOLOv1 využivá SGD (stochastický gradientní sestup) jako svůj optimizer. Ztrátová funkce je rozdělena na více částí, z nichž každá penalizuje různé věci, konkrétně nepřesné predikce bounding boxu, měr spolehlivosti a pravděpodobnosti příslušnosti k nesprávným třídám.
Ztrátová funkce je definovaná vztahem
\begin{equation}
\begin{aligned}
\text{Loss} = &\; \lambda_{\text{coord}} \sum_{i=0}^{S^2} \sum_{j=0}^{B} \mathbbm{1} _{ij}^{\text{obj}} \left[ (x_i - \hat{x}_i)^2 + (y_i - \hat{y}_i)^2 \right] \\
&+ \lambda_{\text{coord}} \sum_{i=0}^{S^2} \sum_{j=0}^{B} \mathbbm{1}_{ij}^{\text{obj}} \left[ (\sqrt{w_i} - \sqrt{\hat{w}_i})^2 + (\sqrt{h_i} - \sqrt{\hat{h}_i})^2 \right] \\
&+ \sum_{i=0}^{S^2} \sum_{j=0}^{B} \mathbbm{1}_{ij}^{\text{obj}} \left( C_i - \hat{C}_i \right)^2 \\
&+ \lambda_{\text{noobj}} \sum_{i=0}^{S^2} \sum_{j=0}^{B} \mathbbm{1}_{ij}^{\text{noobj}} \left( C_i - \hat{C}_i \right)^2 \\
&+ \sum_{i=0}^{S^2} \mathbbm{1}_i^{\text{obj}} \sum_{c \in \text{classes}} \left( p_i(c) - \hat{p}_i(c) \right)^2,
\end{aligned}
\end{equation}

kde:
\begin{itemize}
    \item $\lambda_{\text{coord}}$ a $\lambda_{\text{noobj}}$ jsou regularizační parametry, které slouží k vyvážení váhy buněk, které obsahují a které neobsahují objekty, 
    \item $\mathbbm{1}_{i}^{\text{obj}}$ je indikátor, zda se objekt nachází v buňce $i$,
    \item $\mathbbm{1}_{ij}^{\text{obj}}$ je indikátor, zda se objekt nachází v $j$tém bounding boxu buňky $i$,
    \item $\mathbbm{1}_{ij}^{\text{noobj}}$ je indikátor, zda se objekt nenachází v $j$tém bounding boxu buňky $i$,
    \item $(x_i, y_i, w_i, h_i)$ a $(\hat{x}_i, \hat{y}_i, \hat{w}_i, \hat{h}_i)$ jsou souřadnice ground-truth a predikovaných bounding boxů,
    \item $C_i$ a $\hat{C}_i$ jsou skutečné a predikované hodnoty míry spolehlivosti,
    \item $p_i(c)$ a $\hat{p}_i(c)$ jsou skutečné a predikované pravdědpodobnosti, že se objekt třídy $c$ nachází v buňce $i$.
\end{itemize}

Architektura YOLOv1 umožňovala zpracování v reálném čase díky svému jednoduchému zpracování. V porovnání s dosavadními state-of-the-art modely jako je Fast R-CNN měla však vyšší chyby při lokalizaci. Síť měla tendenci mít problémy s malými objekty nebo objekty seskupenými těsně vedle sebe. 

\subsection{YOLOv8}
V roce 2023 představila společnosti Ultralytics model YOLOv8~\cite{Jocher_Ultralytics_YOLO_2023}. V tuto dobu se architektura jednotlivých modelů detekce objektů, včetně modelu YOLOv8, dělí na tři části -- backbone, neck a head~\cite{YOLOReview}.  Backbone je základní část, jejíž účelem je extrakce z příznaků ze snímku. Backbone obvykle obsahuje několik konvolučních vrstev, které slouží k inicializaci a transformaci surových dat do vysokoúrovňových vlastností. Neck je část sítě umístěná mezi backbone a head. Její úkolem je dále zpracovávat příznaky získané z backbone 
se zaměřením na posílení prostorových a sémantických informací.
Head je koncová část sítě, která využívá příznaky zpracované v neck k provádění specifických úloh, jako je lokalizace a predikce tříd.

Architektura modelu YOLOv8 vychází z modelu YOLOv5, který byl vydán v roce 2020 stejnou společností Ultralytics~\cite{YOLOv5}. Diagram architektury modelu YOLOv8 je zobrazen na obrázku~\ref{fig:yolov8dia}.
Backbone YOLOv8 vychází z architektury CSPDarknet53, která byla poprvé představena v modelu YOLOv4~\cite{bochkovskiy2020yolov4}.
Modul C3, který využival modul YOLOv5, byl vyměněn za modul C2f (cross-stage partial bottleneck with two
convolutions). Modul C2f se skládá ze Split části, která rozděluje vstup na dvě části, z nichž jedna prochází Bottleneck modulem. Rozdělený vstupy
jsou nakonec spojeny a procházejí jednou konvoluční vrstvou. Na konci bottleneck části se nachází vrstva SPPF (Spatial Pyramid Pooling-Fast), která slouží ke zrychlení inference. Mezi další změny patří například výměna první konvoluční vrstvy z velikosti 6 $\times$ 6 na 3 $\times$ 3 a odstranění objectness větve~\cite{yolosumm}.
Všechny konvoluční vrstvy využívají SiLU (Sigmoid Linear Unit)~\cite{silu} aktivační funkci definovanou 
\begin{equation}
    \text{SiLU}(x) = x \cdot \sigma(x),
\end{equation}
kde 
\begin{equation}
\sigma(x) = \frac{1}{1 + e^{-x}}.
\end{equation}

\begin{figure}[!htb]
        \centering
    \includegraphics[width=0.9\textwidth]{images/therory/make-05-00083-g017.png}
        \captionsetup{justification=centering}
	\caption[Diagram YOLOv8.]{Diagram YOLOv8.~\cite{openmmlab_yolov8_2023}}
	\label{fig:yolov8dia}
\end{figure}

Konstrukce YOLOv8 využívá anchor-free systém, tedy přímo predikuje střed bounding boxu objektu. Předchozí YOLO modely, počínaje od YOLOv2, nepredikovaly přímo středy bounding boxů, ale jejich systém byl založený na výpočtu posunů vůči sadě referenčních boxů (anchor)~\cite{YOLOReview}.

Část head je rozdělená na dvě větve (tzv. decoupled head), z nichž každá se zaměřuje na jiný problém. Větev klasifikační se zameřuje na predikci třídy a větev regresní na predikci souřadnic bounding boxu. 

Klasifikační větev využívá ve své ztrátové funkci binární relativní entropii (Binary Cross-Entropy) BCE, která je definovaná vzorcem
\begin{equation}
\text{BCE} = -\left(y_c \cdot \log(\hat{p}) + (1 - y_c) \cdot \log(1 - \hat{p})\right),
\end{equation}
kde \( y_c = 1 \) jestli označený objekt je třídy \( c \) a \( \hat{p} \) je predikovaná pravděpodobnosti příslušnosti k třídě \( c \).

Regresní větev využívá ztrátovou funkci, která se skládá ze dvou částí. Konkrétně z  $\text{Loss}_{\text{CIoU}}$ a $\text{Loss}_{\text{DFL}}$. Výsledná ztrátová funkce celé sítě je pak váženým součtem všech ztrátových funkcí z obou větví, konkrétně

\begin{equation}
\text{Loss} = 0,5 \cdot \text{Loss}_{\text{BCE}} + 7,5 \cdot \text{Loss}_{\text{CIoU}} +1,5 \cdot\text{Loss}_{\text{DFL}},
\end{equation}

CIoU (Complete Intersection Over Union) rozšiřuje koncept IoU o další členy, které pomáhají lépe sladit predikované a ground-truth bounding boxy. CIoU je definovaná vztahem

\begin{equation}
\text{CIoU} = 1 - \text{IoU}^{\text{truth}}_{\text{pred}} + \frac{\rho^2(b, b_{gt})}{c^2} + \alpha \cdot v,
\end{equation}
kde:
\begin{itemize}
\item \(\text{IoU}^{\text{truth}}\) je IoU mezi predikovaným bounding boxem \(b\) a ground-truth bounding boxem \(b_{gt}\),
\item \(\rho(b, b_{gt})\) označuje Euklidovskou vzdálenost mezi středy bounding boxů \(b\) a \(b_{gt}\),
\item \(c\) je délka diagonály nejmenšího obdélníku, který pokrývá \(b\) i \(b_{gt}\),
\item \(v\) je míra poměru \(b\) a \(b_{gt}\) dána vztahem \(v = \frac{4}{\pi^2} (\text{arctan} \frac{w_{gt}}{h_{gt}} - \text{arctan} \frac{w}{h})^2\),
\item \(\alpha\) is a trade-off parametr dán vztahem \(\alpha = \frac{v}{(1 - \text{IoU}^{\text{truth}}_{\text{pred}}) + v}\).
\end{itemize}

DFL (Distribution Focal Loss) pomáhá modelu, aby se prioritně učil ze souřadnic ground-truth bounding boxů, které jsou blízko vybrané buňce.
\begin{equation}
\text{DFL}(P_i, P_{i+1}) = -(y_{i+1} - y)\log(P_i) + (y - y_i)\log(P_{i+1}),
\end{equation}
kde 
     \( P_i \) and \( P_{i+1} \) jsou pravděpodobnosti, že \( y_i \) a \( y_{i+1} \) jsou hranice buňky, které jsou nejbližší vybrané buňce $y$.~\cite{9792391}

YOLOv8 modely k detekci objektů existují v 5 různých variantách, které se odlišují počtem parametrů, a tedy i jejich kvalitou detekce, která je na úkor výpočetních prostředků. V tabulce~\ref{tab:yolcomp} je zobrazeno porovnání všech 5 modelů a vybraných metrik. Metrika mAP@0.5:0.95 byla měřena na MS COCO datasetu. MS COCO je rozsáhlý dataset určený pro trénování algoritmů strojového vidění. 
MS COCO dataset je známý svou rozmanitostí a obsahem více než 300 tisíc obrázků a přes 2,5 milionu anotovaných objektů z 80 různých tříd~\cite{coco-dataset}.
Na obrázku~\ref{fig:yolovcomods} je graf, který porovnává závislosti mAP@0.5:0.95 měřených na MS COCO datasetu na počtu parametrů a rychlosti s předchozími YOLO modely.

\begin{table}[h!]
\centering
\captionsetup{justification=centering}
\caption[Porovnání vybraných metrik mezi modely YOLOv8.]{Porovnání vybraných metrik mezi modely YOLOv8.~\cite{Jocher_Ultralytics_YOLO_2023}}
\label{tab:yolcomp}
\shorthandoff{-}
\begin{tabular}{|l|c|c|c|}
\hline
\multirow{2}{*}{\textbf{Model}} & \multicolumn{3}{c|}{\textbf{Metric}} \\ \cline{2-4} 
                                & \textbf{mAP@0.5:0.95} & \textbf{\# parametrů (mil.)} & \textbf{FLOPs (mld.)} \\ \hline \hline
\textbf{YOLOv8n}                & 0,373                  & 3,2                 & 8.7                \\ \hline
\textbf{YOLOv8s}                & 0,449                  & 11,2                & 28,6               \\ \hline
\textbf{YOLOv8m}                & 0,502                  & 25,9                & 78,9               \\ \hline
\textbf{YOLOv8l}                & 0,529                  & 43,7                & 165,2              \\ \hline
\textbf{YOLOv8x}                & 0,539                  & 68,2                & 257,8              \\ \hline
\end{tabular}
\end{table}

\begin{figure}[!htb]
        \centering
    \includegraphics[width=0.8\textwidth]{images/therory/yolo-comparison-plots.png}
        \captionsetup{justification=centering}
	\caption[Porovnání YOLOv8 s předchozími verzemi.]{Porovnání YOLOv8 s předchozími verzemi.~\cite{Jocher_Ultralytics_YOLO_2023}}
	\label{fig:yolovcomods}
\end{figure}

\section{Evaluační metriky}
K vyhodnocení algoritmů detekce objektů se používají specifické metriky pro měření jejich kvality. Mezi základní pojmy patří 
\begin{itemize}
    \item \textbf{True Positive (TP)}: správná detekce objektu, predikovaný bounding box odpovídá ground-truth bounding boxu (skutečný bounding box z anotace),
    \item \textbf{False Positive (FP)}: detekce neexistujícího objektu nebo špatná predikce umístění bounding boxu,
    \item \textbf{False Negative (FN)}: chybějící detekce, model nepredikoval žádný bounding box, který by odpovídal ground-truth bounding boxu.~\cite{Metrics}
\end{itemize}

\begin{table}[h!]
\centering
\captionsetup{justification=centering}
\caption{Matice záměn v detekci objektů.}
\label{tab:confusion}
\shorthandoff{-}
\begin{tabular}{|c|c|c|c|}
\hline
\multicolumn{2}{|c|}{} & \multicolumn{2}{c|}{\textbf{Skutečnost}} \\ \cline{3-4} 
\multicolumn{2}{|c|}{} & \textbf{Objekt} & \textbf{Pozadí} \\ \hline
\multirow{2}{*}{\textbf{Predikce}} & \textbf{Objekt} & TP & FP \\ \cline{2-4} 
 & \textbf{Pozadí} & FN &  \\ \hline
\end{tabular}
\end{table}

Znázornění uvedených pojmů je uvedeno v tabulce~\ref{tab:confusion}. V kontextu detekce objektů se nepoužívá pojem \textit{True Negative} (TN), který se používá v jiných oblastech strojového učení, jelikož existuje nekonečně bounding boxů ohraničující části pozadí, které by by byly správné.

Ke zjištění, zda predikovaný bounding box skutečně odpovídá nějakému ground-truth bounding boxu se používá IoU (Intersection over Union)~\cite{Metrics}. Pokud IoU je vyšší než daný práh $t$, tak je predikovaný bounding box považován za správný. IoU je definován vztahem 
\begin{equation}
\text{IoU} = \frac{\text{area}(B_p \cap B_{gt})}{\text{area}(B_p \cup B_{gt})},
\end{equation}
kde $\text{area}(B_p \cap B_{gt})$ obsah průniku predikované bounding boxu \( B_{p} \) a ground-truth bounding boxu \( B_{gt} \). Proměnná $\text{area}(B_p \cup B_{gt})$ je naopak obsah jejich sjednocení. Na obrázku~\ref{fig:iou} je zobrazena ukázka vybraných IoU hodnot.

\begin{figure}[!h]
        \centering
    \includegraphics[width=0.7\textwidth]{images/therory/iou_scores.png}
        \captionsetup{justification=centering}
	\caption[Obrázková ukázka IoU hodnot.]{Obrázková ukázka IoU hodnot.~\cite{ronny_iou}}
	\label{fig:iou}
\end{figure}

K vyhodnocení kvality modelů se hojně využívají metriky precision a recall~\cite{Metrics}. 
Míra metriky precision udává, jak dobře model dokáže správně identifikovat relevantní objekty mezi všemi detekovanými objekty. Je vypočítán podle vzorce
\begin{equation}
\text{Precision} = \frac{\text{TP}}{\text{TP} + \text{FP}}.
\end{equation}
Recall vyjadřuje, jak dobře model dokáže najít všechny relevantní objekty ve snímku. Je definován vztahem
\begin{equation}
\text{Recall} = \frac{\text{TP}}{\text{TP} + \text{FN}}.
\end{equation}

Average Precision (AP) je nejpoužívanější metrika pří měření kvality modelu~\cite{COCODatasetFormat}.
AP poskytuje vyvážené hodnocení mezi metrikami precision a recall. Vysoké hodnoty AP indikují, že model dokáže udržet vysokou precision i recall, což znamená, že model nejen správně identifikuje většinu relevantních objektů, ale také minimalizuje počet FN a FP. Metrika AP odhaduje obsah pod \textit{precision-recall} křivkou. K výpočtu se používá 11bodová interpolace (11-point interpolation). Výpočet probíhá přes zjištění maximální hodnoty precision skrz 11 hodnot recall z množiny $\{0; 0,1; \ldots; 1\}$ a následně dojde k jejich zprůměrování. AP je definována vztahem
\begin{equation}
\text{AP} = \frac{1}{11} \sum_{R \in \{0; 0,1; \ldots; 1\}} P_{interp}(R),
\end{equation}
kde
\begin{equation}
 P_{interp}(R) = \max_{\tilde{R}:\tilde{R} \geq R} P(\tilde{R}),
\end{equation}
kde $P(\tilde{R})$ je hodnota precision ($P$) v bodě, kde je recall ($R$) roven $\tilde{R}$.

Nakonec Mean Average Precision (mAP) je průměrem AP přes všechny třídy v datasetu. Výpočet probíhá přes vztah
\begin{equation}
\text{mAP} = \frac{1}{N} \sum_{i=1}^{N} \text{AP}_i,
\end{equation}
kde \(\text{AP}_i \) označuje AP  \( i \)té třídy a \( N \) je celkový počet tříd.~\cite{Metrics}

Často jsou používané metriky mAP na konkrétním prahu IoU. Konkrétním případem je například mAP@0.5, což je mAP počítaný na prahu 0,5, a je používán pro evaluaci modelu na PASCAL VOC datasetu~\cite{pascal-voc-2012}. Dalším příkladem je mAP@0.5:0.95, která je průměrem mAP pro hodnoty IoU z množiny $\{0,5; 0,55; \ldots, 0,95\}$. mAP@0.5:0.95 se používá jako hlavní metrika k evaluaci modelů na MS COCO datasetu~\cite{COCODatasetFormat}.

\section{Perspektivní transformace}
Perspektivní transformace je technika v počítačovém vidění, která se používá k transformaci perspektivy obrazu. 
Perspektivní transformace je reprezentována transformační maticí $\mathbf{H}$, která má rozměry 3$\times$3. Tato matice transformuje odpovídající body ze zdrojového snímku do cílového snímku.
Matice $\mathbf{H}$ splňuje vztah
\begin{equation}
    t_i
    \begin{pmatrix}
    x'_i \\ {y'_i} \\ 1
    \end{pmatrix} 
    = \mathbf{H} \begin{pmatrix}
    x_i \\ {y_i} \\ 1
    \end{pmatrix} 
\end{equation}

pro 4 dvojice odpovídajících pevných bodů ze zdrojového snímku a cílového snímku, tedy platí $i \in \{0, 1, 2, 3\}$. Dále
\( (x_i, y_i) \) jsou souřadnice bodu ve zdrojovém snímku, \( (x'_i, y'_i) \) jsou souřadnice odpovídajícícho bodu v cílovém snímku, a $t_i$ je škála.~\cite{opencv_imgproc_transform}

K transformaci jednoho bodu se využívá funkce $P_{\mathbf{H}}: \mathbb{R}^2 \to \mathbb{R}^2$ definovaná vztahem
\begin{equation}
(x', y')  = P_{\mathbf{H}}(x, y) = \left ( \frac{H_{11} x + H_{12} y + H_{13}}{H_{31} x + H_{32} y + H_{33}} ,
\frac{H_{21} x + H_{22} y + H_{23}}{H_{31} x + H_{32} y + H_{33}} \right ),
\end{equation}

kde \( (x, y) \) jsou souřadnice bodu ve zdrojovém snímku, \( (x', y') \) jsou souřadnice odpovídajícího bodu v cílovém snímku a $\mathbf{H}=\begin{pmatrix}
H_{11} & H_{12} & H_{13} \\
H_{21} & H_{22} & H_{23} \\
H_{31} & H_{32} & H_{33}
\end{pmatrix}$ je transformační matice.~\cite{opencv_imgproc_transform}

Pro výpočet transformační matice $\mathbf{H}$ jsou potřeba čtyři body z původního obrazu a jejich odpovídající body ve výstupním obraze. Tyto body by neměly být kolineární, aby byla transformace platná. Matice 
$\mathbf{H}$
je určena řešením soustavy lineárních rovnic, která vychází z korespondence mezi body ve vstupním a výstupním obraze. 
Na obrázku~\ref{fig:perp} je zobrazena ukázka perspektivní transformace na papíře.

\begin{figure}[!h]
        \centering
    \includegraphics[width=0.7\textwidth]{images/therory/getperspective_transform_01.jpeg}
        \captionsetup{justification=centering}
	\caption[Ukázka perspektivní transformace.]{Ukázka perspektivní transformace.~\cite{rosebrock2014perspective}}
	\label{fig:perp}
\end{figure}